#include <stdio.h>
#include <time.h>
#define M 10
#define N 256
int main()
{
	srand(time(0));
	char strarr[M][N] = { 0 };
	int i = 0;
	char *temp;
	char *pchar[M];
	for (int j = 0; j < M; j++)
	{
		pchar[j] = &strarr[j];
	}
	printf("Enter strings!\n");
	for (i; i < M; i++)
	{
		fgets(strarr[i], N, stdin);
		if (strlen(strarr[i]) == 1)
		{
			i--;
			break;
		}
	}
	for (int j = 0; j < M; j++)
	{
		int ran1 = rand() % i;
		int ran2 = rand() % i;
		temp = pchar[ran1];
		pchar[ran1] = pchar[ran2];
		pchar[ran2] = temp;
	}
	printf("\nAnswer!\n");
	for (int j = 0; j < M; j++)
	{
		printf("%s", pchar[j]);
	}
	return 0;
}