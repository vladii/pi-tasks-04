#include <stdio.h>
#define M 10
#define N 256
int main()
{
	char strarr[M][N];
	int i = 0;
	printf("Enter strings!\n");
	for (i; i < M; i++)
	{
		fgets(strarr[i], N, stdin);
		if (strlen(strarr[i]) == 1)
		{
			i--;
			break;
		}
	}
	printf("\nAnswer!\n");
	for (i; i >= 0; i--)
	{
		for (int j = strlen(strarr[i]); j >= 0; j--)
		{
			printf("%c", strarr[i][j]);
		}
	}
	printf("\n");
	return 0;
}