#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include<stdio.h>
int main(){
	int num;
	char line[N][N] = { 0 };
	printf("Enter the number of lines, please\n");
	scanf("%d", &num);
	printf("\nEnter %d lines, please:\n",num);
	for (int i = 0; i <= num; i++)
		fgets(line[i], 256, stdin);
	printf("\n Your string:\n");
	for (int i = 1; i <= num; i++)
		printf(" %d = %s", i , line[i]);
	printf("\n Result: \n");
	for (int i = num; i > 0; i--)
		printf(" %d = %s", i, line[i]);
	printf("\n\n");
	return 0;
}