#include <stdio.h>
#include <string.h>

#define N 10
#define M 256

int main(){
	int i = 0, lenght;
	char str[N][M];

	printf("Enter strings\n");
	
	for (i = 0; i < N; ++i){
		fgets(str[i], 256, stdin);
		if (str[i][0] == '\n'){
			break;
		}
	}

	printf("Output is\n\n");
	for (lenght = 1; lenght <= M; ++lenght){
		for (i = 0; i < N; ++i){
			if (strlen(str[i]) == lenght)
				printf("%s", str[i]);
		}
	}

	return 0;
}