#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX 4

void enterLine(char *arr[])
{
    int i;
    for( i = 0; i < MAX; i++)
        fgets(arr+i, 256, stdin);
}

void outRandLine (char *arr[])
{
    int i = rand()%MAX;
        puts(arr+i);
}

int main()
{
    srand(time(0));
    
    char str[MAX][256] = {0};
    
    printf("Please, enter %d line\n", MAX);
    
    enterLine(str);
    outRandLine(str);
    
    return 0;
}
