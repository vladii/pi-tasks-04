#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 4
char **str;

void readStr(void)
{
    int i;
    for (i = 0; i < MAX; i++)
        fgets(str[i], 256, stdin);
}

void outStr(void)
{
    int i, index;
    for (i = 0; i < MAX; i++)
    {
        index = rand()%(MAX-i);
        puts(str[index]);
        if (index != (MAX-1-i))
            str[index]=str[MAX-1-i];
    }
}

int main()
{
    srand(time(0));
    
    int i;
    
    str=(char**)malloc(MAX*sizeof(char*));
    for (i = 0; i < MAX; i++)
        str[i] = (char*)malloc(256*sizeof(char));
   
    readStr();
    outStr();
    
    free(str);
    
    return 0;
}
