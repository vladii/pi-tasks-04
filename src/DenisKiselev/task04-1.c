#include <stdio.h>
#define N 256
#define SIZE 5	//quantity of strings

void Inverse(char (*string)[N])
{
	int i;
	printf("\nInverse of array:\n");
	for (i=SIZE-1; i!=-1; i--)
		printf("%s",string[i]);
}

int main()
{
	char string[SIZE][N];
	int i;
	printf("Enter %d lines: \n",SIZE);
	for (i=0; i<SIZE; i++)
		fgets(string[i],N,stdin);
	Inverse(string);
	return 0;
}