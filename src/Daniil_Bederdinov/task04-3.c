#define N 5
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare(char * a, char * b)
{
    size_t fa = strlen((char *)a);
    size_t fb = strlen((char *)b);
    return (fa < fb) - (fa > fb);
}
int main()
{
    char str[N][256];
    int i = 0, j = 0;
    int strcount;
    while (i < N)
    {
        fgets(str[i], 256, stdin);
        i++;
    }
    qsort(str, N, 256, compare);
    strcount = i;
    for (i = strcount; i >= 0; i--)
    {
        j = 0;
        while (str[i][j] > 30)
        {
            printf("%c", str[i][j]);
            j++;
        }
        printf("\n");
    }
    return 0;
}