#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 10
#define L 20

int enterStrings(char (*s)[L])
{
    int i=0;
    for(i=0;i<N;i++)
    {
        fgets(s[i],L,stdin);
        s[i][strlen(s[i])-1]='\0';
        if (strlen(s[i])==0)
            return i;
    }
    return i;
}
void printStrings(char(*s)[L],int kol)
{
    int i;
    for(i=0;i<kol;i++)
    {
        printf("%s\n",s[i]);
    }
}
void printRandOrder(char(*s)[L], int kol)
{
    int i,ind;
    int order[N];
    for(i=0;i<kol;i++)
    {
        order[i]=i;
    }
    for(i=kol;i>0;i--)
    {
        ind = rand()%i;
        printf("%s\n",s[order[ind]]);
        s[order[ind]][0]='\0';
        order[ind]=order[i-1];

    }
}

int main(void)
{
    char strings[N][L];
    int kol;
    srand(time(0));
    kol=enterStrings(strings[0]);
    printRandOrder(strings[0],kol);
    return 0;
}
