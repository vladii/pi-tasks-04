#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 15

int main()
{
	srand(time(0));
	int i, j,temp;
	i = 1;
	char str[N][256];
	int arr[N];
	fgets(str[0], 256, stdin);
	while (str[i - 1][0] > 10 && i < N)
	{
		fgets(str[i], 256, stdin);
		i++;
	}
	int StrNum, RandomStr;
	StrNum = i - 2;
	for (i = 0; i < N; i++)
		arr[i] = i;
	for (i = StrNum;i > 0;i--)
	{
		RandomStr = rand() % (i+1);
		j = 0;
		while ((unsigned int)str[arr[RandomStr]][j] > 16)
		{
			putchar(str[arr[RandomStr]][j]);
			j++;
		}
		temp = arr[RandomStr];
		arr[RandomStr]=arr[i ];
		arr[i ] = temp;
		printf("\n");
	}
	j = 0;
	while ((unsigned int)str[arr[0]][j] > 16)
	{
		putchar(str[arr[0]][j]);
		j++;
	}
	return 0;
}